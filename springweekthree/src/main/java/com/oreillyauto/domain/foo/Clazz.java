package com.oreillyauto.domain.foo;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;

@Entity
@Table(name="CLASSES")
public class Clazz implements Serializable {
	private static final long serialVersionUID = -5996834027120348470L;
    public Clazz() {}
    
    @Id 
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "class_id", columnDefinition = "INTEGER")
    private Integer classId;

    @Column(name = "class_name", columnDefinition = "VARCHAR(128)")
	private String className;

    @Column(name = "instructor", columnDefinition = "VARCHAR(128)")
	private String instructor;
    
    @Column(name = "days", columnDefinition = "VARCHAR(8)")
	private String days;
    
    @Column(name = "start", columnDefinition = "VARCHAR(8)")
	private String start;
    
    @Column(name = "duration", columnDefinition = "INTEGER")
	private String duration;
    
    // Bidirectional Mapping - not needed
    //@OneToMany(fetch = FetchType.EAGER, mappedBy="clazz") 
    
    // Unidirectional Mapping
    @OneToMany(fetch = FetchType.EAGER)
    @JoinColumn(name = "class_id")
    @OrderBy("tx_id ASC")
    private List<Pupil> pupilList = new ArrayList<Pupil>();
    
	public Integer getClassId() {
		return classId;
	}

	public void setClassId(Integer classId) {
		this.classId = classId;
	}

	public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}

	public String getInstructor() {
		return instructor;
	}

	public void setInstructor(String instructor) {
		this.instructor = instructor;
	}

	public String getDays() {
		return days;
	}

	public void setDays(String days) {
		this.days = days;
	}

	public String getStart() {
		return start;
	}

	public void setStart(String start) {
		this.start = start;
	}

	public String getDuration() {
		return duration;
	}

	public void setDuration(String duration) {
		this.duration = duration;
	}

	public List<Pupil> getPupilList() {
		return pupilList;
	}

	public void setPupilList(List<Pupil> pupilList) {
		this.pupilList = pupilList;
	}

	@Override
	public String toString() {
		return "Clazz [classId=" + classId + ", className=" + className + ", instructor=" + instructor + ", days="
				+ days + ", start=" + start + ", duration=" + duration + "]";
	}
    
}
