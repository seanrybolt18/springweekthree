package com.oreillyauto.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class FetchController extends BaseController {
	
	@GetMapping(value = "/fetch")
	public String getFetch() {
		return "fetch";
	}
    
}
