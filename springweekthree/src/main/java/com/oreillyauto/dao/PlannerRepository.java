package com.oreillyauto.dao;

import org.springframework.data.repository.CrudRepository;

import com.oreillyauto.dao.custom.PlannerRepositoryCustom;
import com.oreillyauto.domain.projects.Planner;

public interface PlannerRepository extends CrudRepository<Planner, Integer>, PlannerRepositoryCustom {
    
}
