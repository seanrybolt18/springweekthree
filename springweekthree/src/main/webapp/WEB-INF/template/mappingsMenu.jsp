<%@ include file="/WEB-INF/layouts/include.jsp"%>

<nav class="navbar bg-light">
	<ul class="nav nav-pills flex-column">
		<li>
		    <a class="nav-link" 
		       href="<c:url value='/mappings/orders/unidirectional-many-to-one' />">
		    	Unidirectional Many-to-One
		    </a>
		</li>
		<li>
		    <a class="nav-link" 
		       href="<c:url value='/mappings/orders/unidirectional-one-to-many' />">
		    	Unidirectional One-to-Many
		    </a>
		</li>
		<li>
		    <a class="nav-link" 
		       href="<c:url value='/mappings/orders/bidirectional-one-to-many' />">
		    	Bidirectional One-to-Many
		    </a>
		</li>
		<li>
		    <a class="nav-link" 
		       href="<c:url value='/mappings/orders/bidirectional-many-to-many' />">
		    	Bidirectional Many-to-Many
		    </a>
		</li>
		<li>
		    <a class="nav-link" 
		       href="<c:url value='/mappings/addIntern' />">
		    	Add Intern
		    </a>
		</li>
	</ul>
</nav>