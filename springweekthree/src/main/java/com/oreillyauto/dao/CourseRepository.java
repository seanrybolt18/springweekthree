package com.oreillyauto.dao;

import org.springframework.data.repository.CrudRepository;

import com.oreillyauto.dao.custom.CourseRepositoryCustom;
import com.oreillyauto.domain.university.Course;

/*
 *  CrudRepository<Example, Integer>  <== IMPORTANT: The integer is the datatype of your GUID (PK) on your table  
 */ 
public interface CourseRepository extends CrudRepository<Course, Integer>, CourseRepositoryCustom {
    // Add your interface methods here (that ARE Spring Data methods)
    
}
