package com.oreillyauto.domain.orders;
import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="ORDERS")
public class Order implements Serializable {
	private static final long serialVersionUID = -5996834027120348470L;
    public Order() {}
    
    @Id 
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "order_id", columnDefinition = "INTEGER")
    private Integer orderId;

    @Column(name = "first_name", columnDefinition = "VARCHAR(64)")
    private String firstName;
    
    @Column(name = "last_name", columnDefinition = "VARCHAR(64)")
    private String lastName;
	
	public Integer getOrderId() {
		return orderId;
	}

	public void setOrderId(Integer orderId) {
		this.orderId = orderId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	@Override
	public String toString() {
		return "Facility [order_id=" + orderId + ", firstName=" + firstName + ", lastName=" + lastName + "]";
	}

}
